#ifndef STACK_H
#define STACK_H

/* �����, ����������� ������� ������ ��������� ������ ����*/

#include <iostream>
using namespace std;

template <typename T>
class Stack
{
public:
    Stack(int = 10);                  // �� ��������� ������ ����� ����� 10 ���������
    ~Stack();                         // ����������

    void push(const T&);      // ��������� ������� � ������� �����
    void pop();               // ������� ������� �� ������� �����
    void printStack();        // ����� ����� �� �����
    T at(int) const;        // n-� ������� �� ������� �����
    int getStackSize() const; // �������� ������ �����
    T* getPtr() const;        // �������� ��������� �� ����
    int getTop() const;       // �������� ����� �������� �������� � �����

private:
    T* stackPtr;                      // ��������� �� ����
    const int size;                   // ������������ ���������� ��������� � �����
    int top;                          // ����� �������� �������� �����
};

///////////////////////����������///////////////////////////


// ����������� �����
template <typename T>
Stack<T>::Stack(int maxSize) :
    size(maxSize) // ������������� ���������
{
    stackPtr = new T[size]; // �������� ������ ��� ����
    top = 0; // �������������� ������� ������� �����;
}

// ���������� �����
template <typename T>
Stack<T>::~Stack()
{
    delete[] stackPtr; // ������� ����
}

// ������� ���������� �������� � ����
template <typename T>
void Stack<T>::push(const T& value)
{
    // ����� �������� �������� ������ ���� ������ ������� �����
    if (top < size)
        stackPtr[top++] = value; // �������� ������� � ����
}

// ������� �������� �������� �� �����
template <typename T>
void Stack<T>::pop()
{
    // ����� �������� �������� ������ ���� ������ 0
    if (top > 0)
        stackPtr[--top]; // ������� ������� �� �����
}

// ������� ���������� n-� ������� �� ������� �����
template <class T>
T Stack<T>::at(int nom) const
{
    // �������� �������� ������ ���� ������ ������ ��������������
    if (nom <= top)
        return stackPtr[top - nom]; // ������� n-� ������� �����

    return T();
}

// ����� ����� �� �����
template <typename T>
void Stack<T>::printStack()
{
    for (int ix = top - 1; ix >= 0; ix--)
        cout << stackPtr[ix] << endl;
}

// ������� ������ �����
template <typename T>
int Stack<T>::getStackSize() const
{
    return size;
}

// ������� ��������� �� ���� (��� ������������ �����������)
template <typename T>
T* Stack<T>::getPtr() const
{
    return stackPtr;
}

// ������� ������ �����
template <typename T>
int Stack<T>::getTop() const
{
    return top;
}


#endif // STACK_H
