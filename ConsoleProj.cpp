﻿#include "Stack.h"
#include "Animal.h"

int main()
{
    //homework_18
    cout << "homework_18" << endl;

    int size = 6;
    Stack<char> stackSymbol(size);
    cout << "The stack with "<< size << " elements:" << endl;

    // помещаем элементы в стек
    stackSymbol.push('H');
    stackSymbol.push('e');
    stackSymbol.push('l');
    stackSymbol.push('l');
    stackSymbol.push('o');
    stackSymbol.push('!');
    
    // печать стека
    stackSymbol.printStack();

    cout << endl << "Delete the last input element:" << endl;
    stackSymbol.pop();

    // печать стека
    stackSymbol.printStack();

    int pos = 2;
    cout << endl << "The element at position " << pos << " is: " << stackSymbol.at(pos) << endl << endl;

    //homework_19
    cout << "homework_19" << endl;
    Animal* animals[] = { new Cat(), new Dog(), new Cow() };

    for (auto animal : animals)
        animal->Voice();

    return 0;
}